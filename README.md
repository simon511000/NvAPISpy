Overview
-
This is a DLL replacement for `nvapi.dll` or `nvapi64.dll` to allow sniffing and then reverse engineering of I2C transactions sent by a manufacturers control program to a Nvidia GPU.

This works by exposing a modified NvAPI_QueryInterface that will return the address of our own sniffing function if a I2C read or I2C write is requested and will otherwise will return the address of the real NvAPI DLL.

When an application performs a call that NvAPISpy overrides, it will create a log file in `C:\NvAPISpy\nvapi_XXXX.txt` containing the function calls along with the parameters.

This DLL can be used in one of two ways - global replacement or local override.

Instructions for global replacement:
-
- Make a new folder at `C:\NvAPISpy\`
- Reboot into safe mode
- For 32-bit NvAPI:
  - Open `C:\Windows\SysWOW64\`
  - Rename `nvapi.dll` to `nvapi_orig.dll`
  - Copy NvAPISpy's version of nvapi.dll to `C:\Windows\SysWOW64\`
- For 64-bit NvAPI:
  - Open `C:\Windows\System32\`
  - Rename `nvapi64.dll` to `nvapi64_orig.dll`
  - Copy NvAPISpy's version of nvapi64.dll to `C:\Windows\System32\`
- Reboot
- Run OEM software
- When done reverse engineering, remove the NvAPISpy `nvapi.dll`/`nvapi64.dll` from the paths they were installed and rename `nvapi_orig.dll`/`nvapi64_orig.dll` back to `nvapi.dll`/`nvapi64.dll`.    Some applications may not work with the modified DLL in place, and it may cause issues with anti-tamper, anti-cheat, etc. on games.  Always revert back when you're done reverse engineering and you won't have any issues.

Instructions for local override:
-
- Make a new folder at `C:\NvAPISpy\`
- Navigate to the application you wish to install the DLL override for.
- Copy NvAPISpy's `nvapi.dll` and/or `nvapi64.dll` (depending on architecture of application) to the application's directory, same directory as the .exe file you wish to capture calls from.
- Run the application
- When done reverse engineering, remove the NvAPISpy `nvapi.dll` and/or `nvapi64.dll` from the application directory.